Feature: Google page search testing

  Scenario: Check navigate to google.co.in
    Given I navigate to "https://www.google.co.in/"
    Then I am on page "https://www.google.co.in/"

  Scenario Outline: Verify search result on a single page
    Given I navigate to "https://www.google.co.in/"
    When I type "<keyword>" in search box
    And I click on search
    Then The user is redirected to search result page
    And Search box contains "<keyword>"
    And Search result page contains "<results_count>"
    Examples:
      | keyword  | results_count |
      | hello    | 7             |
      | rabbitmq | 10            |

  Scenario: Verify search button when nothing to search
    Given I navigate to "https://www.google.co.in/"
    When I click on search
    Then I am on page "https://www.google.co.in/"

  Scenario Outline: Verify search result "Did you mean" displayed
    Given I navigate to "https://www.google.co.in/"
    When I type "<keyword>" in search box
    And I click on search
    Then The user is redirected to search result page
    And Search box contains "<keyword>"
    And "Did you mean" box is displayed with text "Acestea sunt rezultatele pentru"
    Examples:
      | keyword |
      | hllo    |
      | engne   |
