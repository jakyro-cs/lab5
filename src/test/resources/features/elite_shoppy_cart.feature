#Feature: Check Elite Shoppy cart
#
#  Background: User is on elite shoppy
#    Given I navigate to "https://loving-hermann-e2094b.netlify.app/womens.html"
#
#  # QAF - 528
#  Scenario: Verify Woman's page add product to card
#    Given Cart is empty
#    When I add items to cart
#      | name                      | count |
#      | Sleeveless Solid Blue Top | 1     |
#    Then Cart should contain
#      | name                      | count |
#      | Sleeveless Solid Blue Top | 1     |
#
#  # QAF - 529
#  Scenario: Verify Cart popup quantity change
#    Given Cart contains
#      | name                      | count |
#      | Sleeveless Solid Blue Top | 1     |
#    When I change quantity of "Sleeveless Solid Blue Top" to "2"
#    Then Total price of "Sleeveless Solid Blue Top" should be "$281.98"
#    And Subtotal should be "$281.98"
#
#  # QAF - 530
#  Scenario: Verify Cart popup invalid quantity change
#    Given Cart contains
#      | name                      | count |
#      | Sleeveless Solid Blue Top | 1     |
#    When I change quantity of "Sleeveless Solid Blue Top" to "a"
#    Then Error should appear
#
#  # QAF - 532
#  Scenario: Verify Cart popup remove item
#    Given Cart contains
#      | name                      | count |
#      | Sleeveless Solid Blue Top | 1     |
#    When I click on cart icon
#    And I remove items
#      | name                      |
#      | Sleeveless Solid Blue Top |
#    Then Cart should be empty
#
#  # QAF - 533
#  Scenario: Verify Cart Quantity by clicking on "Add to Cart"
#    Given Cart contains
#      | name                      | count |
#      | Sleeveless Solid Blue Top | 1     |
#    When I add items to cart
#      | name                      | count |
#      | Sleeveless Solid Blue Top | 1     |
#    Then Cart should contain
#      | name                      | count |
#      | Sleeveless Solid Blue Top | 2     |
#
#  # QAF - 534
#  Scenario: Verify Cart Pup-up disappears when click outside
#    Given Cart pop-up is open
#    When I click on body
#    Then Cart should not be visible
