package model;

public class Product {
    private String name;
    private Integer count;

    public Product(String name, Integer count) {
        this.name = name;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public Integer getCount() {
        return count;
    }
}
