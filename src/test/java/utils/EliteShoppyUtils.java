package utils;

import io.cucumber.java.DataTableType;
import model.Product;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import static utils.Utils.safeSleep;
import static utils.WebDriverManager.await;
import static utils.WebDriverManager.awaitList;

public class EliteShoppyUtils {

    @DataTableType
    public Product defineProduct(Map<String, String> entry) {
        Integer count = entry.get("count") == null ? null : Integer.valueOf(entry.get("count"));
        return new Product(entry.get("name"), count);
    }

    public static void clearCart() {
        openCart();
        List<WebElement> remove = awaitList(getCartPopup(), By.cssSelector(".minicart-remove"));
        remove.forEach(WebElement::click);
    }

    public static void addToCart(Product product) {
        Optional<WebElement> prod = getItemByName(product.getName());
        if (prod.isPresent()) {
            WebElement btn = getAddToCartButton(prod.get());
            for (int i = 0; i < product.getCount(); i++)
                btn.click();
        } else {
            throw new IllegalStateException(String.format("Product with name `%s` not found.", product.getName()));
        }
    }

    public static void removeFromCart(Product product) {
        Optional<WebElement> cartItem = getCartItemByName(product.getName());
        if (cartItem.isPresent()) {
            getRemoveButton(cartItem.get()).click();
        } else {
            throw new IllegalStateException(String.format("Product with name `%s` not found.", product.getName()));
        }
    }

    public static void openCart() {
        WebElement btn = await(By.cssSelector(".w3view-cart"));
        btn.click();
        safeSleep(500);
    }

    public static WebElement getCartPopup() {
        return await(By.cssSelector("#PPMiniCart"));
    }

    public static void setQuantityOfCartItemTo(WebElement item, String quantity) {
        WebElement input = await(item, By.cssSelector(".minicart-quantity"));
        input.clear();
        input.sendKeys(quantity);
        item.click();
        safeSleep(500);
    }

    public static void addProductsToCart(List<Product> products) {
        products.forEach(EliteShoppyUtils::addToCart);
    }

    public static Optional<WebElement> getCartItemByName(String name) {
        return getCartItems()
                .stream()
                .filter(byCartProductName(name))
                .findFirst();
    }

    public static Optional<WebElement> getItemByName(String name) {
        return awaitList(By.cssSelector(".product-men"))
                .stream()
                .filter(byProductName(name))
                .findFirst();
    }

    public static List<WebElement> getCartItems() {
        WebElement cartPopup = getCartPopup();
        return awaitList(cartPopup, By.cssSelector(".minicart-item"));
    }

    public static String cartProductPrice(WebElement el) {
        return await(el, By.cssSelector(".minicart-price")).getText();
    }

    public static String cartProductName(WebElement el) {
        return await(el, By.cssSelector(".minicart-details-name a")).getText();
    }

    public static Integer cartProductQuantity(WebElement el) {
        WebElement input = await(el, By.cssSelector(".minicart-quantity"));
        return Integer.valueOf(input.getAttribute("value"));
    }

    private static String productName(WebElement el) {
        return await(el, By.cssSelector(".item-info-product a")).getText();
    }

    private static WebElement getRemoveButton(WebElement el) {
        return await(el, By.cssSelector(".minicart-remove"));
    }

    private static WebElement getAddToCartButton(WebElement cartItem) {
        return await(cartItem, By.cssSelector(".snipcart-details input[name='submit']"));
    }

    private static Predicate<WebElement> byProductName(String name) {
        return (el) -> productName(el).equals(name);
    }

    private static Predicate<WebElement> byCartProductName(String name) {
        return (el) -> cartProductName(el).equals(name);
    }

}
