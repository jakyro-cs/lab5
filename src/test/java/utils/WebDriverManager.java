package utils;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.nio.file.Paths;
import java.util.List;

import static utils.Utils.safeSleep;

public class WebDriverManager {
    public static final Integer RETRY_COUNT = 5;
    public static WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver",
                Paths.get("src/test/resources/chromedriver/chromedriver.exe").toString());
        if (driver == null) {
            driver = new ChromeDriver();
        }
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.close();
            driver.quit();
            driver = null;
        }
    }

    public static void navigate(String url) {
        driver.navigate().to(url);
    }

    public static String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public static WebElement await(By selector) {
        return await(driver, selector);
    }

    public static WebElement await(SearchContext context, By selector) {
        int i = RETRY_COUNT;
        while (i > 0) {
            try {
                return context.findElement(selector);
            } catch (Exception ignored) {
            }
            i--;
            safeSleep(100);
        }
        return context.findElement(selector);
    }

    public static List<WebElement> awaitList(By selector) {
        return awaitList(driver, selector);
    }

    public static List<WebElement> awaitList(SearchContext context, By selector) {
        int i = RETRY_COUNT;
        while (i > 0) {
            try {
                return context.findElements(selector);
            } catch (Exception ignored) {
            }
            i--;
            safeSleep(100);
        }
        return context.findElements(selector);
    }
}
