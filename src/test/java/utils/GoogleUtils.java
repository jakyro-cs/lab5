package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static utils.WebDriverManager.await;
import static utils.WebDriverManager.awaitList;

public class GoogleUtils {

    public static WebElement getSearchBox() {
        return await(By.cssSelector(".RNNXgb input.gLFyf.gsfi"));
    }

    public static WebElement getSearchButton() {
        return await(By.cssSelector(".FPdoLc.tfB0Bf input.gNO89b"));
    }

    public static List<WebElement> getSearchResults() {
        return awaitList(By.cssSelector("#rso > div[class='g']"));
    }

    public static WebElement getDidYouMeanBox() {
        return await(By.cssSelector(".med span.gL9Hy"));
    }
}
