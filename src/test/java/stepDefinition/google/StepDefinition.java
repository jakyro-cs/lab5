package stepDefinition.google;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

import java.util.List;

import static utils.GoogleUtils.*;
import static utils.WebDriverManager.getCurrentUrl;

public class StepDefinition {

    @When("I type {string} in search box")
    public void typeKeywordsIntoSearchBox(String keyword) {
        WebElement searchBox = getSearchBox();
        searchBox.sendKeys(keyword);
    }

    @When("I click on search")
    public void iClickOnSearch() {
        getSearchButton().click();
    }

    @Then("The user is redirected to search result page")
    public void checkUserIsRedirectedToResultPage() {
        boolean startsWith = getCurrentUrl().startsWith("https://www.google.co.in/search");
        Assert.assertTrue("User is not redirected to search page", startsWith);
    }

    @When("Search box contains {string}")
    public void checkSearchBpxContains(String keyword) {
        WebElement searchBox = getSearchBox();
        Assert.assertEquals("Search keywords do not match", keyword, searchBox.getAttribute("value"));
    }

    @Then("Search result page contains {string}")
    public void checkResultPageContainsCount(String countStr) {
        int count = Integer.parseInt(countStr);
        List<WebElement> results = getSearchResults();
        Assert.assertEquals("Search result counts do not match", count, results.size());
    }

    @Then("\"Did you mean\" box is displayed with text {string}")
    public void checkDidYouMeanDisplayed(String text) {
        WebElement didYouMeanBox = getDidYouMeanBox();
        Assert.assertTrue("Did you mean box not displayed", didYouMeanBox.isDisplayed());
        Assert.assertEquals("Did you mean box texts do not match", text, didYouMeanBox.getText());
    }
}
