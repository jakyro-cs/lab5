package stepDefinition.elite;

import io.cucumber.java.en.When;
import model.Product;
import utils.EliteShoppyUtils;

import java.util.List;

public class CartActionSteps {

    @When("I add items to cart")
    public void whenAddItemsToCart(List<Product> products) {
        EliteShoppyUtils.addProductsToCart(products);
    }

    @When("I remove items")
    public void removeItems(List<Product> products) {
        products.forEach(EliteShoppyUtils::removeFromCart);
    }

}
