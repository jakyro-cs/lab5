package stepDefinition.elite;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import model.Product;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Optional;

import static utils.EliteShoppyUtils.*;
import static utils.WebDriverManager.await;

public class ValidationSteps {
    @Then("Total price of {string} should be {string}")
    public void checkPriceOfProduct(String product, String price) {
        Optional<WebElement> opItem = getCartItemByName(product);
        if (opItem.isPresent()) {
            WebElement item = opItem.get();
            Assert.assertEquals(price, cartProductPrice(item));
        } else {
            throw new IllegalStateException(String.format("Product with name `%s` not found.", product));
        }
    }

    @Then("Subtotal should be {string}")
    public void checkPriceOfProduct(String price) {
        WebElement subtotal = await(getCartPopup(), By.cssSelector(".minicart-subtotal"));
        Assert.assertEquals(String.format("Subtotal: %s USD", price), subtotal.getText());
    }

    @Then("Cart should be empty")
    public void validateCartEmpty() {
        Assert.assertEquals("Cart items count", 0, getCartItems().size());
    }

    @Then("Cart should contain")
    public void cartShouldContain(List<Product> products) {
        List<WebElement> items = getCartItems();
        Assert.assertEquals("Cart items count", products.size(), getCartItems().size());
        for (int i = 0; i < items.size(); i++) {
            WebElement item = items.get(i);
            Product product = products.get(i);
            Assert.assertEquals("Product name", product.getName(), cartProductName(item));
            if (product.getCount() != null) {
                Assert.assertEquals(
                        String.format("Product %s count", product.getName()),
                        product.getCount(),
                        cartProductQuantity(item)
                );
            }
        }
    }

    String oldItemContent = null;
    WebElement item = null;

    @When("I change quantity of {string} to {string}")
    public void changeQuantityOfProduct(String product, String quantity) {
        Optional<WebElement> opItem = getCartItemByName(product);
        if (opItem.isPresent()) {
            item = opItem.get();
            oldItemContent = item.getAttribute("innerHTML");
            setQuantityOfCartItemTo(item, quantity);
        } else {
            throw new IllegalStateException(String.format("Product with name `%s` not found.", product));
        }
    }

    @Then("Error should appear")
    public void errorShouldAppear() {
        if (item != null) {
            String newContent = item.getAttribute("innerHTML");
            String oldContent = oldItemContent;
            oldItemContent = null;
            item = null;
            Assert.assertNotEquals(oldContent, newContent);
        } else {
            throw new IllegalStateException("item can not be null at this step. Check previous step");
        }
    }

}
