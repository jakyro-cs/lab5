package stepDefinition.elite;

import io.cucumber.java.en.Given;
import model.Product;
import utils.EliteShoppyUtils;

import java.util.List;

import static utils.EliteShoppyUtils.clearCart;

public class PreconditionSteps {

    @Given("Cart is empty")
    public void givenCartIsEmpty() {
        clearCart();
    }

    @Given("Cart contains")
    public void cartContains(List<Product> products) {
        clearCart();
        EliteShoppyUtils.addProductsToCart(products);
    }

}
