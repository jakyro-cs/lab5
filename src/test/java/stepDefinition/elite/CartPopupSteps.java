package stepDefinition.elite;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import static utils.EliteShoppyUtils.*;

public class CartPopupSteps {

    @When("I click on cart icon")
    public void clickOnCart() {
        openCart();
    }

    @Then("Cart pop-up is visible")
    public void cartIsVisible() {
        WebElement popup = getCartPopup();
        Assert.assertTrue(popup.isDisplayed());
    }

    @Given("Cart pop-up is open")
    public void cartPupUpIsOpen() {
        openCart();
    }

    @Then("Cart should not be visible")
    public void cartIsNotVisible() {
        WebElement popup = getCartPopup();
        Assert.assertFalse(popup.isDisplayed());
    }

}
