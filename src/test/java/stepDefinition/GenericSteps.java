package stepDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;

import static utils.WebDriverManager.*;

public class GenericSteps {

    @Given("I navigate to {string}")
    public void toHomepage(String url) {
        navigate(url);
    }

    @When("I click on body")
    public void clickOnBody() {
        await(By.cssSelector("body")).click();
    }

    @Then("I am on page {string}")
    public void checkOnPage(String url) {
        Assert.assertEquals("User is not redirected to page", url, getCurrentUrl());
    }
}
